#!/bin/bash
build(){
    npm run build-scss||exit 100
}

clean(){
    npm ci --only=prod||exit 100
}

copy_build(){
    stat $1 &>/dev/null&&rm -rf $1
    mkdir $1
    cp -r --target-directory=$1 $(cat files)||exit 1
    cd $1&&npm i --only=prod &&cd ..||exit 1 #Install modules to the build directory
}

safedel(){
    stat $1&>/dev/null&&rm -rf $1||return 0
}

make_tarball(){
    name_i="tarball_build"
    safedel $name_i
    safedel $1
    echo "Copying files to ${name_i}"
    copy_build $name_i||exit 50
    echo "Generating tarball from ${name_i} to $1"
    tar --exclude=.* --exclude=stages.sh --exclude=test --exclude=test.js \
        --exclude=node_modules/**.md --exclude=node_modules/**.html --exclude node_modules/**.png --exclude=node_modules/**.json \
        -C $name_i -cf $1 $(cat files) node_modules ||exit 2
    stat $1 >/dev/null&&echo "Tarball created on $1"||exit 1
    rm -rf $name_i
}

extract_tarball(){
    stat $1>/dev/null||exit 255
    safedel $2
    mkdir $2
    echo "Prepared to extract"
    tar -C $2 -xf $1||exit 200
    stat $2>/dev/null&&echo "Content extracted OK"
}

set -e
export PATH=node_modules/.bin:$PATH #Ensure that working node_modules is in the path

if [ $1 == "create" ]; then
    build||exit 100
    if [ "$2" ];then
        copy_build $2||exit 50
    else
        copy_build build||exit 50
    fi
    echo "Created build on the target folder"
fi

if [ $1 == "devcreate" ]; then
    npm i --only=dev
    build||exit 50
    if [ "$2" ];then
        copy_build $2||exit 50
    else
        copy_build build||exit 50
    fi
    echo "Deploy created to target folder"
fi

if [ $1 == "tarball" ]; then
    npm i --only=dev
    build||exit 50
    name="$2"
    if [ "$2" == "" ];then name="build.tar"; fi
    make_tarball $name
fi

if [ $1 == "tarball-deploy" ]; then
    npm i --only=dev
    build||exit 50
    name="$2.tar"
    out="$2"
    [ "$2" == "" ]&&name="build.tar"&&out="build"
    make_tarball $name||exit 2
    echo "Extracting tarball"
    extract_tarball $name $out||exit 2
    safedel $name
fi
