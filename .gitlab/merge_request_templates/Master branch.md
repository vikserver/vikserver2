# New Merge Request
Thanks for contributing. Please take a minute and fulfill this form
Please, check one or more boxes and remove the others.

## Checklist (Don't remove items)
- [ ] Node modules aren't uploaded, just included in package.json
- [ ] Only uses `npm` as package manager
- [ ] Everything that has changed is working. (Else make a merge request to other branch than master)
- [ ] Lighthouse test done (add it as PDF or JSON to the Merge Request)

## What changed (Select one of them)
- [ ] New feature
- [ ] Modified (or updated) dependency
- [ ] Issue fix -> Fix #
- [ ] Template changed
- [ ] Docs modified
- [ ] Other


## Details
What has changed in general

## More
- [ ] Dependencies changed
  - [ ] New dependencies:
  - [ ] Removed dependencies:
  - [ ] I'm sure of the new deps are loaded like is specified at CONTRIBUTING.md
- [ ] Loader has changed (Explain the reason on Details)
