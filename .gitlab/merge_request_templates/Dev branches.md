# New Pull Request
This template is only intended for development branches (like beta and other feature branches)  
Thanks for contributing. Please take a minute and fulfill this form
Please, check one or more boxes and delete the rest.

## Checklist (Don't remove items)
- [ ] Node modules aren't uploaded, just included in package.json
- [ ] Only uses `npm` as package manager

## What changed (Select one of them)
- [ ] New feature
- [ ] Modified (or updated) dependency
- [ ] Issue fix -> Fix #
- [ ] Template changed
- [ ] Docs modified
- [ ] Other 

#### Details
What has changed in general

## More
- [ ] Dependencies changed
  - [ ] New dependencies:
  - [ ] Removed dependencies: 
  - [ ] I'm sure of the new deps are loaded like is specified at CONTRIBUTING.md
- [ ] Loader has changed (Explain the reason in Details)
