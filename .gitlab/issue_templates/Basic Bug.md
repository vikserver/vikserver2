# New Bug report

## Thanks for reporting a new bug, please delete this line to keep the issue clean.

## Details
Describe here what happened to you when using Vikserver. Include more info and it will take less time to fix it.
