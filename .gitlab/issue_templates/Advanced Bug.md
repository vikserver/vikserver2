# New Issue report
## Please, select one or more option/s and delete unchecked lines (this too)

## Frontend (ensure you have done the following)
- I'm using the `[fill this]` branch and I've pulled latest commit before creating this issue.
- I've done a `npm i` before reporting.

## Frontend server
- What server are you running?
  - [ ] `viksrv.tk` website.
  - [ ] Branched `viksrv.tk` website (like `beta.viksrv.tk`).
  - [ ] NodeJS (built-in).
  - [ ] NodeJS (My own server) `you know the code` Specify the GitHub URL if is there
  - [ ] Nginx
  - [ ] Apache
  - [ ] Other:

## Backend server
The issue can be related to the backend. If you know that is a backend issue put it on [vikserver/vikserver-backend](https://gitlab.com/vikserver/vikserver-backend).

- [ ] Untouched (connected to the default backend)
- [ ] Cloned from [vikserver/vikserver-backend](https://gitlab.com/vikserver/vikserver-backend)
- [ ] Forked [vikserver/vikserver-backend](https://gitlab.com/vikserver/vikserver-backend). Put the repo here if is public.
  - [ ] Using SSL on both (Frontend and backend)
  - [ ] Not using SSL
  - [ ] Using SSL on `frontend` and not in `backend`

## Details
More details about the Issue (e.g. call stack).
