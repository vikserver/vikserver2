# New Feature Request
## Please, check one or more boxes and remove the others.

## Please, ensure you have done the following
- I've pulled latest commit on the branch `[fill this]` before submitting this.
- I've done `npm install` in order to update the module dependencies.

- Which type of feature is it
  - [ ] Security
  - [ ] Style
  - [ ] Speedup
  - [ ] Other

## Details
Describe in a detailed way the new feature you are requesting.
