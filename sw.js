'use strict';
//MASTER BRANCH
//ServiceWorker v1.0 Stable
//Scripts necesarios para el ServiceWorker

/* eslint-env serviceworker*/
//Ajustes del ServiceWorker
var cacheName="vikserver-v"+1.0;
var urlsToCache=[
    'cont/estructura.json',
    'cont/home-vue.html',
    'cont/login.html',
    'cont/modal-add.html',
    'cont/modal-idiomas.html',
    'lang/es.json',
    'lang/en.json'
];
var cacheList={
    protos:[
        "http:",
        "https:"
    ],
    nocache:[
    ],
    cacheProhibit:[
        "https://vikserver-backend.herokuapp.com"
    ]
};

function cacheAllowed(a){
    if(cacheProhibit(a))return false;
    let url=new URL(a);
    for(let i in cacheList.nocache){
        if(!cacheList.hasOwnProperty(i))continue;
        let noc=cacheList.nocache[i];
        let match=url.href.match(noc);
        if(match)return false;
    }
    return true;
}

function protoAllowed(a){
    let url=new URL(a);
    if(cacheList.protos.includes(url.protocol))return true;
    return false;
}

function cacheProhibit(a){
    //Check protocol
    if(!protoAllowed(a))return true;
    let url=new URL(a);
    for(let p in cacheList.cacheProhibit){
        if(!cacheList.cacheProhibit.hasOwnProperty(p))continue;
        let c=cacheList.cacheProhibit[p];
        if(url.href.match(c))return true;
    }
    return false;
}


//Eventos que maneja el ServiceWorker
this.addEventListener("install", event=>{
    console.log(">> Instalando ServiceWorker");
    event.waitUntil(Promise.all([
        caches.open(cacheName).then(cache=>{
            return cache.addAll(urlsToCache);
        })
    ]));
});
/////////////////////
this.addEventListener("activate", event=>{
    console.log(">> Activando ServiceWorker");
    event.waitUntil(
        async ()=>{
            let lista=[];
            (await caches.list()).forEach(a=>{
                lista.push(a.url);
            });
            return caches.addAll(lista);
        }
    );
});
/////////////////////
this.addEventListener("fetch", event=>{
    event.respondWith(
        cacheHandler(event.request)
    );
});
//////////////////////
async function cacheHandler(request){
    if(!protoAllowed(request.url))return fetchOnly(request);
    if(cacheProhibit(request.url))return fetchOnly(request);
    let cacheMode=request.cache;
    if(cacheMode=="no-store"){
        return fetchOnly(request);
    }
    let cache=await caches.open(cacheName);
    let cachedResponse=await cache.match(request);
    switch(cacheMode){
        case "reload":
            return fetchAndCache(request);
        case "no-cache":
            return fetchAndCache(request).catch(e=>{
                console.error(e);
                if(cachedResponse)return cachedResponse
                throw e;
            });
        case "force-cache":
            return cachedResponse||fetchAndCache(request);
        case "only-if-cached":
            if(cachedResponse)return cachedResponse
            throw new Error("Operating in only-if-cached mode, but there isn't any match");
    }
    if(cachedResponse){
        let allow=cacheAllowed(request.url,request.cache); //Not allowed to return cache except of offline mode
        if(!allow)console.log("Cache is not allowed for "+request.url);
        if(allow||!navigator.onLine){
            return cachedResponse;
        }else{
            return fetchAndCache(request);
        }
    }else{
        if(!navigator.onLine)console.warn(`${request.url} not cached and we're offline`);
        if(cacheProhibit(request.url))return fetchOnly(request);
        return fetchAndCache(request);
    }
    function fetchOnly(req){
        return fetch(req);
    }
    async function fetchAndCache(req){
        let resp=await fetchOnly(req);
        return saveCache(req.url,resp);
    }
    async function saveCache(a,b){
        if(b.status<200||b.status>299){ //Refuse to cache other responses than 2XX
            if(b.type!="opaque"){ //Don't log opaque responses
                console.log("Refusing to cache "+a+" due to status "+b.status);
            }
            return b;
        }
        let cache=await caches.open(cacheName);
        await cache.put(a,b.clone());
        return b;
    }
}
///////////////////////////////////////////////////////////////

//Manejo de los mensajes al SW
this.addEventListener("message", event=>{
    var msg=event.data;
    console.log(msg);
    return mhandler(msg).then(a=>event.ports[0].postMessage(a));
});

/*eslint-disable require-await*/
async function mhandler(a){ //Here the async is not useless as the handle requires it to return a Promise
    async function cache(b){
        let cache=await caches.open(cacheName);
        if(b.tipo=="put"){
            let a1=b.datos.url||b.datos;
            let a2=b.datos.init;
            let req=new Request(a1,a2);
            let response=await fetch(req);
            return cache.put(req, response);
        }else if(b.tipo=="delete"){
            if(b.datos!=undefined) return cache.delete(b.datos);
            let keys=await caches.keys();
            let rtn=[];
            keys.forEach(a=>{
                rtn.push(caches.delete(a));
            });
            return Promise.all(rtn);
        }
    }
    async function db(b){
        if(!self.db){
            console.warn("DB is not initialized");
            return false;
        }
        switch(b.tipo){
            case "open":
                if(self.db.isOpen()) return true;
                return self.db.open();
            case "close":
                return self.db.close();
        }
    }
    async function dbpoolctl(b){
        switch(b.tipo){
            case "init":
                console.log(b);
                if(typeof self.dbpool!="object") return self.dbpool={};
                return self.dbpool;
            case "put":
                self.dbpool[b.usuario]=b.datos;
                return true;
            case "del":
                return delete self.dbpool[b.usuario];
            default:
                return new Error(`Unknown event type ${a.tipo}/${b.tipo}`);
        }
    }
    switch(a.tipo){
        case "cache":
            return cache(a.datos);
        case "db":
            return db(a.datos);
        case "dbpool":
            return dbpoolctl(a.datos);
        default:
            return "Unhandled!!";
    }
}
///////////////////////////////////////////////////////////////
