/* eslint-env node */
"use-strict";
console.log("==> Cargando módulos");
const notifier=require("node-notifier");
const path=require("path");
const {test}=require(path.resolve("test/all.js"));
console.log("==> Iniciando loop test...");
let timeo=10000;
let last;
function loopTest(){
    console.clear();
    console.log(`Último examen: ${last ? last : 'nunca'}`);
    let err=test();
    last=new Date();
    if(err.length>0){
        notifier.notify({
            title: `Hay ${err.length} ${(err.length>1)?"errores":"error"} en los scripts`,
            message: `Errores en ${stringList(err)}`
        });
    }
}
function stringList(e){
    let salida=[];
    e.forEach(sc=>{
        salida.push(sc.path);
    });
    return salida.join(",");
}
loopTest();
setInterval(loopTest, timeo);
