/* eslint-env node */
console.log("==> Cargando módulos");
var fs, babel, path;
try{
    path=require("path");
    fs=require("fs");
    babel=require("@babel/core");
}catch(e){
    console.error(e);
    throw new Error("Uno de los módulos requeridos no está disponible");
}
function test(verbose){
    if(verbose)console.log("==> Iniciando test");
    var errors=[];
    let testFilter=/^.*\.js$/;
    let testFilter2=/^.*\.json$/;
    let testDir=[".", "lang/", "test/", "lib/core", "lib/lang"];
    testDir.forEach(a=>{
        let archivos=fs.readdirSync(a).filter(a=>a.match(testFilter));
        archivos.forEach(b=>{
            let archivo=path.resolve([a, b].join("/"));
            if(verbose)console.log(`Testeando ${archivo}`);
            try{
                babel.transform(fs.readFileSync(archivo, "UTF-8"));
                if(verbose)console.log(`${archivo} testeado correctamente`);
            }catch(e){
                console.error("Error en "+archivo, e.loc);
                e.path=archivo;
                console.log(e.message);
                console.error(e.codeFrame);
                errors.push(e);
            }
        });
        let archivos2=fs.readdirSync(a).filter(a=>a.match(testFilter2));
        archivos2.forEach(b=>{
            let archivo=path.resolve([a,b].join("/"));
            if(verbose)console.log(`Testeando JSON ${archivo}`);
            try{
                JSON.parse(fs.readFileSync(archivo, "UTF-8"));
                if(verbose)console.log(`JSON correcto: ${archivo}`);
            }catch(e){
                console.error("Error en "+archivo);
                e.path=archivo;
                console.error(e.message);
                errors.push(e);
            }
        })
    });
    return errors;
}
module.exports={
    test
};
