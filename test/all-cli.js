#!/bin/env node
/* eslint-env node*/
const path=require("path");
const {test}=require(path.resolve("test/all.js"));
let errors=test(true);
if(errors.length)process.exit(errors.length);
console.log("Tests done, no errors");
process.exit(0);
