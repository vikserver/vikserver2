/* globals Dexie:false */
self.dbload=load("dexie");
class dbjs{
    constructor(){
        this.a=1;
    }

    async initDB(){
        let struct=await fetch("cont/estructura.json").then(b=>b.text()).then(t=>JSON.parse(t));
        let d=struct.db;
        d.forEach(d=>{
            db.version(d.version).stores(d.estructura);
        });
        self.dbjs["estructura"]=struct;
        if(typeof cryptoJS=="undefined"){
            self.dbjs.estructura.userdb.links[0].uid=preCrypto.random();
        }else{
            self.dbjs.estructura.userdb.links[0].uid=cryptoJS.rnd;
        }
        return db.open();
    }

    chk(a){
        return db.Usuarios.where("Usuario").equals(a).toArray(b=>{
            if(b.length==0){
                return false
            }
            return true;
        });
    }

    registrar(a){
        return Promise.all([
            db.Usuarios.add({
                Usuario: a.usuario,
                pgp: a.pgp
            }),
            db.Tablas.add({
                Usuario: a.usuario,
                db: a.db,
                fecha: Date.now()
            })
        ]);
    }

    backup(a){
        return db.Tablas.where("Usuario").equals(a.usuario).toArray().then(a=>{
            return a[0];
        });
    }

    importar(a){
        return db.Usuarios.put({Usuario: a.Usuario, pgp: a.pgp});
    }

    restaurar(a){
        return db.Tablas.put({Usuario: a.Usuario, db: a.db, fecha: a.fecha});
    }

    async dbsync(){
        let db=await this.backup(self.cryptoJS.creds);
        db.hash=sha256(btoa(JSON.stringify(db)));
        let msg=await cryptoJS.firmar(db);
        return msg.data;
    }

    clearUser(name){
        return db.transaction("rw", db.Usuarios, db.Tablas, async ()=>{
            let arr=[];
            let a=db.Usuarios.where("Usuario").equals(name);
            if(await a.count()>0)arr.push(a.delete());
            let b=db.Tablas.where("Usuario").equals(name);
            if(await b.count()>0)arr.push(b.delete());
            if(arr.length)return Promise.all(arr);
            return arr;
        });
    }

    checkDB(user){
        return db.transaction("r!", db.Tablas, async ()=>{
            let c=await db.Tablas.where("Usuario").equals(user).count();
            return c;
        });
    }

    updateKey(info){
        let {user,pgp}=info;
        return db.transaction("rw", db.Usuarios, async ()=>{
            let col=await db.Usuarios.where("Usuario").equals(user);
            if(await col.count()<=0)throw new Error(`No users like ${user} in IDB`);
            await col.modify({pgp});
            return true;
        });
    }

    deleteUser(username){
        return db.transaction("rw", db.Usuarios, db.Tablas,()=>{
            let rq=[
                db.Usuarios.where("Usuario").equals(username).delete(),
                db.Tablas.where("Usuario").equals(username).delete()
            ];
            return Promise.all(rq);
        })
    }
}
class preCrypto{
    static random(){
        return Math.abs(crypto.getRandomValues(new Int32Array(1))[0]).toString(16).substr(0, 6);
    }
}
Promise.all([self.dbload]).then(()=>{
    self.db=new Dexie("vikserver");
    self.dbjs=new dbjs();
    self.dbProgress=self.dbjs.initDB().then(()=>{
        if(typeof window!=="undefined"){
            load("core/crypto.js").then(()=>{
                cryptoJS.importarLocales();
            });
        }
    });
});
