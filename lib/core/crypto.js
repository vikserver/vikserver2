"use-strict";
/* globals openpgp:false */
var secrets;
load("openpgp").then(()=>{
    return openpgp.initWorker({path: 'node_modules/openpgp/dist/openpgp.worker.min.js'});
});
class cryptoJS{
    constructor(a){
        this.in=a;
        if(typeof window.cartera =="undefined"){
            window.cartera={
                Usuarios: {}
            };
        }
    }

    get encKey(){
        return this.propiedades.encKey;
    }

    importar(a){
        window.cartera[a.name]=a.key;
    }

    importarAIDB(a){
        return dbjs.importar(a);
    }

    async importarLocales(){
        let array=await db.Usuarios.toArray();
        array.forEach(b=>{
            window.cartera.Usuarios[b.Usuario]=b.pgp;
        });
        return true;
    }

    importarOnline(b){
        return socketctl.enviarEncriptado({tipo: "req-pgp", msg: b, timeo: 10000}).then(a=>{
            //a->Clave PGP || false
            if(a==false){
                throw new Error(window.lang.crypto.key_nok);
            }
            return this.importarAIDB({Usuario: a.usuario, pgp: {pública: a.pgp.pública, privada: a.pgp.privada}}).then(()=>{
                return this.importarLocales();
            });
        }).catch(e=>{
            console.error("No hemos podido recuperar la clave PGP desde el servidor");
            throw e;
        });
    }

    async auth(a){
        this.creds={
            usuario: a.usuario
        };
        let c;
        if(window.cartera.Usuarios.hasOwnProperty(a.usuario)!=true){
            c=await this.importarOnline(a);
        }else{
            c=a;
        }
        return this.iniciarCredenciales(c);
    }

    async authOnline(a){
        this.creds={
            usuario: a.usuario
        };
        let cred=await this.importarOnline(a).then(()=>{
            return this.iniciarCredenciales(a);
        });
        if(cred!=true) throw new Error(window.lang.crypto.creds_nok);
        return this.req_sync();
    }

    async iniciarCredenciales(a){
        let claves=window.cartera.Usuarios[a.usuario];
        let privateKey=(await openpgp.key.readArmored(claves.privada)).keys[0];
        try{
            secrets=new Secrets({
                pgpArmor: claves,
                pgp: {
                    privada: (await this.decryptKey({privateKey,passphrase:a.contraseña})).key,
                    pública: (await openpgp.key.readArmored(claves.pública)).keys
                }
            });
            return true;
        }catch(e){
            console.log(e);
            return false;
        }
    }

    async registrar(a){
        this.creds={
            usuario: a.usuario
        };
        let key=await openpgp.generateKey({
            userIds: [{name: this.creds.usuario, email: this.creds.usuario+"@users.viksrv.tk"}],
            numBits: 2048,
            passphrase: a.contraseña
        });
        let privateKey=(await openpgp.key.readArmored(key.privateKeyArmored)).keys[0];
        try{
            secrets=new Secrets({
                pgp: {
                    privada: (await this.decryptKey({privateKey,passphrase:a.contraseña})).key,
                    pública: (await openpgp.key.readArmored(key.publicKeyArmored)).keys
                },
                pgpArmor: {
                    pública: key.publicKeyArmored,
                    privada: key.privateKeyArmored
                }
            });
        }catch(e){
            vex.dialog.alert(window.lang.crypto.pgp_rare);
            return;
        }

        let rsp=await socketctl.enviarEncriptado({tipo: "registro", msg: {creds: this.creds, keys: secrets.keys.pgpArmor}});
        if(rsp==false) throw new Error(self.lang.errors.register);
        let crypt=await this.encriptar(dbjs.estructura.userdb, secrets.keys.pgp.pública);
        let dbreg=await dbjs.registrar({usuario: this.creds.usuario, pgp: secrets.keys.pgpArmor, db: crypt.data});
        window.cartera[this.creds.usuario]=secrets.keys.pgpArmor;
        return dbreg;
    }

    sync(){
        return dbjs.backup(this.creds).then(a=>{
            return this.firmar(a, secrets.keys.pgp.privada).then(b=>{
                return socketctl.enviarEncriptado({tipo: "sync", msg: {usuario: this.creds.usuario, db: b.data, fecha: a.fecha}, timeo: 100*1000});
            });
        });
    }

    async req_sync(){
        let rt=await socketctl.enviar({tipo: "req-sync", msg: this.creds, timeo: 100*1000});
        if(!rt)return 0;
        try{
            var msg=await this.desencriptar(rt);
        }catch(e){
            console.error(e);
            await dbjs.clearUser(this.creds.usuario);
            throw new Error("Error al desencriptar la base de datos");
        }

        if(msg.data.length<1){
            return this.reinsertar(dbjs.estructura.userdb);
        }
        return dbjs.restaurar(JSON.parse(msg.data));
    }

    firmar(a, b){
        let c;
        if(typeof a=="object"){
            c=JSON.stringify(a);
        }else{
            c=a;
        }
        return openpgp.sign({
            message: openpgp.message.fromText(c),
            privateKeys: b || secrets.keys.pgp.privada
        });
    }

    detachedSign(msg, key){
        let r=typeof msg=="object"?JSON.stringify(msg):msg;
        let message=openpgp.message.fromText(r);
        return openpgp.sign({
            message,
            privateKeys: key||secrets.keys.pgp.privada,
            detached: true
        }).then(r=>r.signature);
    }

    async encriptar(a, c){
        let b;
        let opciones;
        try{
            if(typeof(a)=="object"){
                b=JSON.stringify(a);
            }else{
                b=a;
            }
            opciones={
                message: openpgp.message.fromText(b),
                publicKeys: c || (await openpgp.key.readArmored(self.cartera.WebSocketServer)).keys
            };
        }catch(e){
            console.error("Error de conversión antes de encriptar");
            throw e;
        }
        return openpgp.encrypt(opciones).catch(e=>{
            console.warn("Error al encriptar los datos");
            throw e;
        });
    }

    async desencriptar(a){
        return openpgp.decrypt({
            privateKeys: [secrets.keys.pgp.privada],
            message: await openpgp.message.readArmored(a)
        });
    }

    decryptKey(p){
        return openpgp.decryptKey(p);
    }

    desencriptar_db(){
        return window.dbjs.backup(this.creds).then(a=>{
            return this.desencriptar(a.db).then(b=>{
                return new home(JSON.parse(b.data));
            });
        });
    }

    recargar_db(){
        return window.dbjs.backup(this.creds).then(a=>{
            return this.desencriptar(a.db).then(b=>{
                return new Promise(resolver=>{
                    resolver(JSON.parse(b.data));
                });
            });
        });
    }

    pre_reinsertar(a){
        let final={
            links: a
        };
        return final;
    }

    reinsertar(a){
        return this.encriptar(a, secrets.keys.pgp.pública).then(a=>{
            return dbjs.restaurar({Usuario: this.creds.usuario, db: a.data, fecha: Date.now()});
        });
    }

    decidir_sync(){
        return dbjs.backup({usuario: this.creds.usuario}).then(b=>{
            return socketctl.enviar({tipo: "decidir_sync", msg: {usuario: this.creds.usuario, db: sha256(btoa(JSON.stringify(b))), fecha: b.fecha}}).then(c=>{
                if(c==false){
                    return false;
                }
                if(c=="servidor"){
                    return this.req_sync();
                }
                if(c=="cliente"){
                    return this.sync();
                }
                console.log(c);
                return;
            });
        });
    }

    get rnd(){
        return Math.abs(this.random()).toString(16).substr(0, 6);

    }

    random(a){
        if(a) return crypto.getRandomValues(new Int32Array(a));
        return crypto.getRandomValues(new Int32Array(1))[0];
    }

    async refresh(msg){
        if(this.creds&&this.creds.usuario){
            try{
                let {data}=await this.desencriptar(msg);
                if(data==this.creds.usuario){
                    let res=await self.main.sync();
                    return res;
                }
            }catch(e){
                return e;
            }
        }
        return 0;
    }

    async checkPassword(pass){
        let hash=sha256(pass);
        if(!secrets)return false;
        let privKey=(await openpgp.key.readArmored(secrets.keys.pgpArmor.privada)).keys[0];
        let res=await privKey.decrypt(hash).catch(()=>false); //True or false
        if(!res)return false;
        return privKey;
    }

    async changePassword(oldPwd, newPwd){
        let privateKey=await this.checkPassword(oldPwd);
        if(!privateKey)return {status: false, error: "Incorrect password"};
        //Start changing key
        let newhash=sha256(newPwd);
        await privateKey.encrypt(newhash);
        let newKey=privateKey.armor();
        await privateKey.decrypt(newhash);
        let detachedSignature=await this.detachedSign(newKey);
        let msg={
            newKey,
            detachedSignature,
            user: this.creds.usuario
        };
        let res=await socketctl.enviarEncriptado({tipo: "change-password", msg}).catch(error=>{
            console.error(error);
            return {status: false, error}
        });
        if(typeof res.status!="undefined")return res;
        if(res.result&&!res.err){
            let pgp={
                privada: newKey,
                pública: secrets.keys.pgpArmor.pública
            };
            await dbjs.updateKey({user: this.creds.usuario, pgp}).catch(e=>res.err=e.toString());
            secrets.pgpArmor=pgp;
        }
        return {status: res.result, error: res.err};
    }

    async checkNewPassword(){
        let {usuario}=this.creds;
        let hash=sha256(secrets.keys.pgpArmor.privada);
        let msg=await this.firmar(hash).then(d=>d.data);
        return socketctl.enviar({tipo: "is-new-key", usuario, msg});
    }

    async updateKey(){
        console.log("UPDATING KEY!");
        let {usuario}=this.creds;
        let msg=await this.firmar(usuario).then(d=>d.data);
        let {result, key, error}=await socketctl.enviar({tipo: "get-new-key", msg, usuario})
            .then(async res=>{
                console.log(res);
                let key=await this.desencriptar(res.key).then(d=>d.data);
                return {...res, key};
            })
            .catch(e=>{
                return {result: false, error: e};
            });
        if(!result||error)return {result,error};
        let pgp={
            privada: key,
            pública: secrets.keys.pgpArmor.pública
        };
        let res=dbjs.updateKey({user: usuario, pgp})
            .then(()=>{
                return result;
            })
            .catch(e=>{
                console.error(e);
                return e;
            });
        if(!res)return res;
        //Reload current armored keys (cannot update open keys)
        secrets.keys.pgpArmor=pgp;
        return res;
    }

    async deleteUser(pwd){
        let pk=this.checkPassword(pwd);
        if(pk==false)return {status: false, error: "Incorrect password"};
        //Create delete-user payload
        let usuario=this.creds.usuario;
        let msg=await this.firmar(usuario).then(d=>d.data);
        let res=await socketctl.enviar({tipo: "delete-user", msg, usuario}).catch(error=>{
            console.error(error);
            return error;
        });
        if(res!==true){
            return {status: false, error: res};
        }
        return dbjs.deleteUser(usuario).then(()=>{
            return {status: true};
        }).catch(e=>{
            return {status: false, error: e};
        });
    }
}
window.cryptoJS=new cryptoJS();
class Secrets{
    constructor(a){
        this.keys={
            pgp: a.pgp,
            pgpArmor: a.pgpArmor
        }
    }
}
