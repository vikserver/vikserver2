/* globals Main:false */
document.querySelector("#login-usuario").addEventListener("change", event=>{
    let user=event.target.value;
    let passDiv=document.querySelector("[data-login-role='passwordDiv']");
    let passInput=passDiv.querySelector("input");
    let loginBtn=document.querySelector("#login-login");
    let regBtn=document.querySelector("#login-registro");
    if(!user){
        passInput.value="";
        addDisabledAttr(true, passInput, loginBtn, regBtn);
        passDiv.classList.add("mdc-text-field--disabled");
        return;
    }
    let chk=dbjs.chk(user);
    chk.then(b=>{
        addDisabledAttr(false, passInput);
        passDiv.classList.remove("mdc-text-field--disabled");
        passInput.focus();
        if(b==true){
            loginBtn.setAttribute("data-login", "local");
            addDisabledAttr(false, loginBtn);
            addDisabledAttr(true, regBtn);
        }else{
            onlineCheck(user).then(b=>{
                loginBtn.setAttribute("data-login", "online");
                if(b){
                    addDisabledAttr(false, loginBtn);
                    addDisabledAttr(true, regBtn);
                }else{
                    self.notify(`Se registrará a '${user}' cuando pulses 'Registrarse'`);
                    addDisabledAttr(true, loginBtn);
                    addDisabledAttr(false, regBtn, passInput);
                    passInput.classList.remove("mdc-text-field--disabled");
                    passInput.focus();
                }
            });
        }
    });
});
document.querySelector("#login-contraseña").addEventListener("keypress", event=>{
    if(event.key=="Enter"){
        document.querySelector("button[data-login]:not(:disabled)").dispatchEvent(new MouseEvent("click"));
    }
});
document.querySelector("#login-login").addEventListener("click", ()=>{
    let passInput=document.querySelector("[data-login-role='passwordDiv'] input");
    let userInput=document.querySelector("[data-login-role='userDiv'] input");
    let loginBtn=document.querySelector("#login-login");
    {
        let preload=document.createElement("link");
        preload.href="cont/home-vue.html";
        preload.rel="preload";
        preload.as="fetch";
        preload.setAttribute("crossorigin","anonymous");
        document.head.appendChild(preload);
    }
    addDisabledAttr(true, loginBtn);
    let credenciales={
        usuario: userInput.value,
        contraseña: passInput.value
    };
    if(credenciales.contraseña.length<1){
        vex.dialog.alert({
            message: window.lang.login.nopass,
            callback: ()=>userInput.dispatchEvent(new InputEvent("change"))
        });
        return;
    }
    credenciales.contraseña=sha256(credenciales.contraseña);
    if(loginBtn.getAttribute("data-login")==="local"){
        cryptoJS.auth(credenciales).then(a=>{
            if(a){
                self.notify(window.lang.login.ok, 1500);
                window.main=new Main();
            }else{
                vex.dialog.alert({
                    message: window.lang.login.pass_nok,
                    callback: ()=>userInput.dispatchEvent(new InputEvent("change"))
                });
            }
        });
    }else{
        cryptoJS.authOnline(credenciales).then(a=>{
            if(a){
                self.notify(window.lang.login.ok, 4000);
                window.main=new Main();
            }else{
                vex.dialog.alert({
                    message: window.lang.login.user_nok,
                    callback: ()=>userInput.dispatchEvent(new InputEvent("change"))
                });
            }
        }).catch(e=>{
            console.error(e);
            if(e.toString().length<4){
                vex.dialog.alert(window.lang.errors.login_What);
            }else{
                vex.dialog.alert(e.toString());
            }
            userInput.dispatchEvent(new InputEvent("change"));
        });
    }
});
document.querySelector("#login-registro").addEventListener("click", ()=>{
    let passInput=document.querySelector("[data-login-role='passwordDiv'] input");
    let userInput=document.querySelector("[data-login-role='userDiv'] input");
    let regBtn=document.querySelector("#login-registro");
    addDisabledAttr(true, regBtn);
    let credenciales={
        usuario: userInput.value,
        contraseña: passInput.value
    };
    if(credenciales.contraseña.length<1){
        vex.dialog.alert({
            message: window.lang.login.nopass,
            callback: ()=>userInput.dispatchEvent(new InputEvent("change"))
        });
        return;
    }
    credenciales.contraseña=sha256(credenciales.contraseña);
    cryptoJS.registrar(credenciales).then(()=>{
        self.notify(window.lang.login.register_ok, 4000);
        window.main=new Main();
    }).catch(e=>{
        console.error(e);
        vex.dialog.alert(window.lang.errors.register);
        console.log("Error al intentar introducir al nuevo usuario en la base de datos");
        userInput.dispatchEvent(new InputEvent("change"));
    });
});

function addDisabledAttr(add, ...elements){ //Add should be true to add attribute, false to remove it, undefined or null to toggle it.
    // eslint-disable-next-line no-nested-ternary
    let fn=add?"setAttribute":add===false?"removeAttribute":"toggleAttribute";
    for(let el of elements){
        el[fn]("disabled", "disabled");
    }
}

function onlineCheck(a){
    return socketctl.enviar({tipo: "chk", msg: a});
}

