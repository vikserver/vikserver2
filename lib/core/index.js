"use-strict";
/* globals Vik:false */
load("core/clases.js"); //No es una dependencia, pero se precarga
load("vue").then(()=>{
    window.floating=new Vue({
        el: "#boton-flotante",
        data: {
            icon: "person",
            hidden: false
        },
        methods: {
            click: function(event){
                window.floatingFunction(event);
            }
        }
    });
})
load("vex").then(()=>{
    vex.defaultOptions.className="vex-theme-plain";
});
document.querySelectorAll(".reemplazar").forEach(element=>{
    var text=element.innerText;
    for(let a in self.lang.index){
        let rx=new RegExp("__"+a+"__", "g");
        text=text.replace(rx, self.lang.index[a]);
    }
    element.innerText=text;
});
fetch("cont/modal-ajustes.html").then(a=>a.text()).then(a=>{
    $("#modal-ajustes").remove();
    $("#contenedor-modales").append(a);
    window.langItems.modals.ajustes=new Vue({
        el: "#modal-ajustes",
        data: window.lang
    });
    window.modalAjustes= new mdc.dialog.MDCDialog.attachTo(document.querySelector("#modal-ajustes"));
    $("#boton-ajustes").on("click", ()=>window.modalAjustes.open());
    $(".mdc-dialog__body--scrollable").css("max-height", $(window).height()/2);
    $(window).on("resize", ()=>{
        $(".mdc-dialog__body--scrollable").css("max-height", $(window).height()/2);
    });
    return Ajustes.Eventos();
});
fetch("cont/modal-usersettings.html").then(a=>a.text()).then(a=>{
    document.querySelector("#contenedor-modales").insertAdjacentHTML('beforeend', a);
    self.langItems.modals.usersettings=new Vue({
        el: "#modal-usersettings",
        data: window.lang
    });
    let el=document.querySelector("#modal-usersettings");
    window.modalUsersettings=new mdc.dialog.MDCDialog(el);
    self.mdc.autoInit(el);
    document.querySelector("#boton-usuario").addEventListener("click", ()=>{
        if(!(self.main instanceof self.Main)){
            return vex.dialog.alert(self.lang.alert.need_login);
        }
        self.modalUsersettings.open();
    });
    return new Usersettings(el);
})
document.querySelector("#invert-theme").addEventListener("click",event=>{
    event.preventDefault();
    let lighturl=new URL("lib/core/theme.css",location).href;
    let darkurl=new URL("lib/core/dark-theme.css",location).href;
    let sel=document.querySelector(`[data-loadlify='${lighturl}']`);
    if(sel){
        load(darkurl,"reapply").then(()=>{
            sel.remove();
            localStorage.setItem("dark",true);
            self.autosetMetaColor();
        });
    }else{
        sel=document.querySelector(`[data-loadlify='${darkurl}']`);
        load(lighturl,"reapply").then(()=>{
            if(sel)sel.remove();
            localStorage.setItem("dark",false);
            self.autosetMetaColor();
        });
    }
});

class Usersettings{
    constructor(base){
        this.base=base;
        this.events();
    }

    events(){
        this.base.querySelector("#us-changePassword>button").addEventListener("click", ()=>this.passwordChange());
        this.base.querySelector("#us-deleteUser>button").addEventListener("click", ()=>this.deleteUser());
    }

    async passwordChange(){
        let oldInput=this.base.querySelector("#us-changePassword [data-chpwd='current']").MDCTextField;
        let old=oldInput.value;
        let chk=cryptoJS.checkPassword(old);
        let npInput=this.base.querySelector("#us-changePassword [data-chpwd='new']").MDCTextField;
        let np=npInput.value;
        let alerts=this.base.querySelector("#us-changePassword [data-role='error']");
        alerts.innerText="";
        if(!await chk){
            oldInput.valid=false;
            return;
        }
        oldInput.valid=true;
        if(np == ""){
            npInput.valid=false;
            return;
        }
        npInput.valid=true;
        let {error,status}=await cryptoJS.changePassword(old, np); //This will change password locally and notify backend
        if(!status||error){
            alerts.innerText=error?error:"An unknown error has ocurred";
        }else{
            self.notify(self.lang.alert.password_changed);
            oldInput.value="";
            npInput.value="";
            self.modalUsersettings.close();
        }
    }

    async deleteUser(){
        let pwdInput=this.base.querySelector("#us-deleteUser [data-deluser='password']").MDCTextField;
        let pwd=pwdInput.value;
        let chk=await cryptoJS.checkPassword(pwd);
        let alerts=this.base.querySelector("#us-deleteUser [data-role='error']");
        alerts.innerText="";
        if(!chk){
            pwdInput.valid=false;
            return;
        }
        pwdInput.valid=true;
        let {error,status}=await cryptoJS.deleteUser(pwd);
        if(!status||error){
            alerts.innerText=error?error:"An unknown error has ocurred";
        }else{
            self.notify(self.lang.alert.account_deleted);
            setTimeout(()=>self.location.reload(), 2000);
            pwdInput.value="";
        }
    }
}

class Spin{
    constructor(a){
        this.id=Number(String(Math.random()).substr(2, 4));
        this.elementos={
            html: `<div class='spin' style='display: none;' data-spin-id='${this.id}'></div>`,
            objetivo: $(a)
        }
        this.elementos["spinner"]=this.elementos.objetivo.append(this.elementos.html).children("[data-spin-id='"+this.id+"']");
        this.elementos.spinner.animate({
            height: Number($(window).height() * 0.05),
            width: Number($(window).height() * 0.05)
        }, 2);
    }

    iniciar(){
        this.elementos.spinner.show(100, "linear");
    }

    parar(){
        this.elementos.spinner.hide(100, "linear");
    }
}
function getLogin(){
    load("cont/login.html","noprefix").then(a=>{
        $("#visor").html(a.text).show(200);
        window.floating.hidden=true;
        window.langItems.visor=new Vue({
            el: "#visor",
            data: window.lang.loginForm
        });
        $("form").children("div").forEach(actual=>{
            new mdc.textField.MDCTextField(actual);
        });
        load("core/login.js");
    }).catch(e=>{
        console.error(e);
        throw new Error(window.lang.errors.login_page);
    });
}
self.floatingFunction=getLogin;

window.Spin=Spin;
load("typedJS").then(X=>{
    if(!$("#motd").length)return;
    new X.exports.Typed("#motd", {
        strings: window.lang.motd,
        typeSpeed: 50,
        backSpeed: 50,
        smartBackspace: true,
        showCursor: false,
        oncomplete: ()=>{$("#motd").hide();}
    });
});
class Ajustes{
    static getProgress(a){
        return $(a).parent().parent().children(".mdc-card__supporting-text").children("div[role='progressbar']");
    }

    static async Eventos(){
        //Only here, settings is allowed to call Dexie directly as dbjs won't be loaded
        await load("dexie");
        $(".tarjeta-ajustes").on("click", event=>{
            this.getProgress(event.target).show(200);
        });
        $("#ajustes-eliminar_db").on("click", event=>{
            return load("dexie")
                .then(()=>{
                    if(typeof swctl !="undefined"){
                        self.swctl.db.close();
                    }
                })
                .then(()=>self.Dexie.delete("vikserver"))
                .then(()=>{
                    self.notify(self.lang.settings.db_deleted, 4000);
                    this.getProgress(event.target).hide(200);
                });
        });
        $("#ajustes-cache_reset").on("click", event=>{
            if(typeof swctl =="undefined"){
                $(event.target).parent().children("div").hide(200);
                return self.notify(self.lang.alert.nosw);
            }
            return self.swctl.cachectl.delete().then(()=>{
                self.notify(self.lang.settings.cache_reset_ok, 4000);
                this.getProgress(event.target).hide(200);
            });
        });
        $("#ajustes-eliminar_datos").on("click", async event=>{
            await load("dexie");
            let tsk=[];
            if(self.swctl){
                tsk.push(self.swctl.db.close());
                tsk.push(self.swctl.cachectl.delete());
            }
            tsk.push(Vik.storage().deleteAll());
            tsk.push(self.Dexie.delete("vikserver"));
            await Promise.all(tsk);
            if(self.swctl){
                await self.swctl.unregister();
            }
            self.notify(self.lang.settings.delete_ok, 4000);
            this.getProgress(event.target).hide(200);
        });
    }
}
