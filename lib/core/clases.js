"use-strict";
class Vik{
    static get(a){
        return fetch(a).then(b=>b.text());
    }

    static post(a){
        return fetch(this.genreq({url: a.url, data: a.data, method: "POST"})).then(a=>{
            if(a.ok) return a.text();
            throw new Error(a.statusText);
        });
    }

    static indexOfProp(a){
        //prop, array, val
        let salida;
        a.array.forEach((b, c)=>{
            if(b[a.prop]==a.val){
                salida=c;
            }
        });
        return salida;
    }

    /*eslint-disable require-await*/
    static storage(){
        let X={
            get: async function(a){
                return localStorage.getItem(a);
            },
            put: async function(a, b){
                return localStorage.setItem(a, b);
            },
            delete: async function(a){
                return localStorage.removeItem(a);
            },
            deleteAll: async function(){
                return localStorage.clear();
            }
        }
        return X;
    }
    /*eslint-enable require-await*/

    static genreq(a){
        let b= new Headers();
        b.append("Content-Type", "application/json");
        let c={
            method: a.method||"GET",
            body: JSON.stringify(a.data),
            headers: b
        };
        return new Request(a.url, c);
    }
}
self.Vik=Vik;

class NotifySystem{
    constructor(a){
        //Set defaults
        this.defaults={
            message: "Empty Notification",
            actionText: "OK",
            timeout: 8000,
            actionHandler: ()=>{/*Noop*/}
        };
        //Create element
        let container=document.createElement("div");
        container.innerHTML=a.element;
        document.querySelector(a.parent).appendChild(container);
        this.selector=document.querySelector(a.selector);
        load("material-components-web-js").then(b=>{
            this.mdc=b.exports.mdc;
            this.snackbar=new this.mdc.snackbar.MDCSnackbar(this.selector);
        });
    }

    notify(a){
        this.configureNotify(a);
        this.snackbar.open();
        return this.snackbar;
    }

    configureNotify(a){
        let obj=this.defaults;
        if(typeof a != "undefined"){
            obj={...this.defaults, ...a};
        }
        this.snackbar.labelText=obj.message;
        this.snackbar.actionButtonText=obj.actionText;
        this.snackbar.timeoutMs=obj.timeout;
    }
}

if(typeof exports=="object"){
    exports.clases={
        Vik,
        Notify: NotifySystem
    };
}
