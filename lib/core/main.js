"use-strict";
/* globals Swctl:false cryptoJS:false */
window.langItems={modals:{}};
window.MDC_Items={menu:{}};
window.shortDomain="https://vshort.tk/";
let {Notify}=exports.clases;
load("cont/notification.html", ["noprefix", "asplain"]).then(x=>{
    self.notificationSystem=new Notify({
        element: x.text,
        parent: "body",
        selector: "#snackbar"
    });
    self.notify=function(a){
        return self.notificationSystem.notify({
            message:a,
            actionText: self.lang.btn.ok
        });
    }
});
let deps=["lang/lang.js"];
load(deps)
    .then(()=>window.loadLang())
    .then(()=>load("core/index.js"));

if('serviceWorker' in navigator && localStorage.nosw!="true"&&location.protocol!="file:"){
    load("core/swctl.js").then(()=>{
        window.swctl=new Swctl({sw: "sw.js", scope: "."});
        console.log("swctl preparado");
    });
}
(function(){
    let menu=new mdc.menu.MDCMenu(document.querySelector(".mdc-menu"));
    menu.setAbsolutePosition(100,100);
    document.querySelector('#boton-mas').addEventListener('click', () => menu.open = !menu.open);
    $(".mdc-simple-menu__items").children().on("click", ()=>{
        menu.open=false;
    });
})();

class main{
    constructor(a){
        this.in=a;
        dbjs.checkDB(cryptoJS.creds.usuario)
            .then(a=>{
                if(a)return;
                return cryptoJS.req_sync();
            })
            .then(()=>{
                this.sync();
                cryptoJS.desencriptar_db().then(b=>{
                    this.home=b;
                    Promise.all([this.home.prog]).then(()=>{
                        this.init();
                    });
                });
            });
    }

    init(){
        let el="#visor";
        let selector=document.querySelector(el);
        selector.innerHTML=this.home.plantilla;
        this.home.db=new Vue({
            el,
            data: {
                links: this.home.db.links,
                lang: window.lang.btn
            },
            methods: this.home.VueEvents.links
        });
        this.home.Eventos.login_ok();
        Vue.nextTick().then(()=>{
            mdc.autoInit(selector);
        });
        document.body.setAttribute("data-login-ok", true);
    }

    async sync(){
        $("[data-sync='indicador']").show(200);
        $("[data-sync='icono']").html("autorenew").addClass("girar");
        window.sync=cryptoJS.decidir_sync();
        var status;
        try{
            status=await window.sync;
            $("[data-sync='icono']").removeClass("girar").html("cloud_done");
            if(status&&status.error){
                console.error(`Error in the backend with code ${status.msg}. ${window.lang.alert[status.msg]}`);
                return 1;
            }
            if(status!=false)window.main.home.recargar();
        }catch(e){
            if(e.name=="offline"){
                $("[data-sync='icono']").removeClass("girar").html("cloud_off");
            }
            $("[data-sync='icono']").removeClass("girar").html("error");
            console.error(e);
            vex.dialog.alert(e.toString().capitalize());
        }
        if(await cryptoJS.checkNewPassword()){
            let {error}=cryptoJS.updateKey();
            if(error)self.notify(error);
        }
        return status;
    }

    público(a,b){
        return new Promise((resolver, rechazar)=>{
            cryptoJS.firmar(b).then(c=>{
                return socketctl.enviarEncriptado({tipo: a,msg:{data: c, usuario: b.usuario}}).then(d=>{
                    if(d.status) return resolver(d);
                    if(d.err) return rechazar(d.err);
                    return resolver(d);
                });
            });
        });
    }
}

window.Main=main;
