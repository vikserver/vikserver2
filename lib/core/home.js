"use-strict";
/* globals Vik:false main:false */
class home{
    constructor(a){
        this.db=a;
        this.fix_editing();
        this.reinsertar();
        this.prog=load(["vue", "cont/home-vue.html"], ["noprefix"]).then(a=>{
            this.plantilla=a[1].text;
        });
        this.Eventos=Eventos;
        this.VueEvents=VueEvents;
        self.floatingFunction=function(){window.modalAdd.open();};
        self.floating.hidden=0;
        self.floating.icon="add";
    }

    fix_editing(){
        let f=this.db.links.filter(l=>l.edit);
        if(f.length<1)return;
        f.forEach(l=>l.edit=0);
    }

    añadir(a){
        a.uid=cryptoJS.rnd;
        this.db.links.push(a);
        return this.reinsertar();
    }

    eliminar(a){
        let b=Vik.indexOfProp({array: this.db.links, prop: "uid", val: a.uid});
        if(b!=undefined){
            this.db.links.splice(b, 1);
            return this.reinsertar();
        }else{
            self.notify(window.lang.alert.link_404, 1000);
        }
    }

    modificar(a){
        let b=Vik.indexOfProp({array: this.db.links, prop: "uid", val:a.uid});
        a.edit=0;
        if(b!=undefined){
            this.db.links[b]=a;
            return this.reinsertar();
        }else{
            self.notify(window.lang.alert.link_404, 1000);
        }
    }

    recargar(b){
        return cryptoJS.recargar_db().then(a=>{
            this.db.links=a.links;
            if(!b)return
            return main.sync();
        });
    }

    reinsertar(){
        return cryptoJS.reinsertar(cryptoJS.pre_reinsertar(this.db.links)).then(()=>{
            return Promise.resolve(this.recargar(true));
        });
    }
}

class Eventos{
    static modal_add(){
        if(self.modalAdd)return self.modalAdd;
        self.modalAdd=new self.mdc.dialog.MDCDialog($("#modal-añadir")[0]);
    }

    static login_ok(){
        this.syncBtn();
        fetch("cont/modal-add.html").then(a=>a.text()).then(a=>{
            $("#modal-añadir").remove();
            $("#contenedor-modales").append(a);
            window.langItems.modals.add=new Vue({
                el: "#modal-añadir",
                data: window.lang.modal_add
            });
            mdc.autoInit(document.querySelector("#modal-añadir"));
            this.modal_add();
            $("#añadir-cancelar").on("click", ()=>{
                $("[data-añadir]").val("").trigger("focusout");
            });
            $("#añadir-aceptar").on("click", ()=>{
                let valores={
                    link: $("input[data-añadir='enlace']").val(),
                    edit: 0,
                    descripción: $("input[data-añadir='desc']").val()
                };
                if(document.querySelector("input[data-añadir='enlace']").checkValidity()==false){
                    return self.notify(window.lang.alert.link_invalid, 5000);
                }
                if(document.querySelector("input[data-añadir='desc']").checkValidity()==false){
                    return self.notify(window.lang.alert.desc_invalid, 2000);
                }
                $("#añadir-cancelar").click();
                main.home.añadir(valores).then(()=>{
                    self.notify(window.lang.alert.link_saved, 1000);
                }).catch(e=>{
                    setTimeout(()=>{
                        vex.dialog.alert(e.toString());
                    }, 1000);
                    console.error(e);
                });
            });
        });
    }

    static syncBtn(){
        let boton=$("[data-sync='indicador']");
        boton.off("click").on("click", ()=>{
            if(boton.children("i").hasClass("girar")){
                self.notify(window.lang.sync.sync_running, 1000);
                return;
            }
            return main.sync();
        });
    }
}
window.home=home;
var VueEvents ={
    links: {
        copiar: function (a){
            if(!navigator.clipboard)return Promise.reject(new Error("Clipboard API is not supported"));
            return navigator.clipboard.writeText(a).then(()=>{
                self.notify(self.lang.alert.copied);
            });
        },
        eliminar: function (target){
            //.links-eliminar
            let uid=$(target).parent().attr("data-uid");
            let id=Vik.indexOfProp({prop: "uid", array: main.home.db.links, val: uid});
            setTimeout(()=>{
                vex.dialog.confirm({
                    message: window.lang.alert.link_will_delete+main.home.db.links[id].link+window.lang.alert.sure_to_continue,
                    callback: a=>{
                        if(a!=true){
                            self.notify(window.lang.alert.wont_change, 500);
                            return;
                        }
                        if(id&&main.home.db.links[id].shared) main.público("del-public", {uid: uid, usuario: cryptoJS.creds.usuario});
                        main.home.eliminar({uid: uid}).then(()=>{
                            self.notify(window.lang.alert.link_deleted, 1000);
                        }).catch(e=>{
                            setTimeout(()=>vex.dialog.alert(e.toString()), 1000);
                            console.error(e);
                        });
                    }
                });
            }, 10);
        },
        editar: function (target){
            let comun=$(target).parent("div").parent("li").children("span");
            let selectores={
                desc: comun.children("span[data-desc]"),
                link: comun.children(".link-container").children("a")
            };
            let id=Vik.indexOfProp({val: $(target).parent().attr("data-uid"), array: self.main.home.db.links, prop:"uid"});
            let datos=self.main.home.db.links[id];
            datos.id=id;
            //Entrar en modo edición
            main.home.db.links[id].edit=1;
            //Añadir las propiedades y eliminar las clases anteriores
            selectores.desc.removeClass("nosel").attr("contenteditable", "true");
            selectores.link.attr("contenteditable", "true");
            //Después de actualizar el DOM establecer los eventos
            main.home.db.$nextTick(()=>{
                let div_ab=$(target).parent().parent().children("div.links-ab");
                function terminarEdit(){
                    $(".links-ab__cancel").off("click");
                    $(".links-ab__confirm").off("click");
                    selectores.link.removeAttr("contenteditable");
                    selectores.desc.removeAttr("contenteditable").addClass("nosel");
                    main.home.db.links[id].edit=0;
                }
                div_ab.children(".links-ab__cancel").on("click", ()=>{
                    selectores.link.text(datos.link);
                    selectores.desc.text(datos.descripción);
                    terminarEdit();
                });
                div_ab.children(".links-ab__confirm").on("click", ()=>{
                    guardarCambios();
                    terminarEdit();
                });
            });
            function guardarCambios(){
                let datosNuevos={
                    link: selectores.link.text(),
                    descripción: selectores.desc.text(),
                    uid: datos.uid,
                    edit:0
                };
                datosNuevos.shared=datos.shared;
                window.main.home.modificar(datosNuevos).then(()=>{
                    self.notify(window.lang.alert.link_modified, 1000);
                    if(!datosNuevos.shared||datosNuevos.shared=="")return;
                    main.público("update-public", {link: datosNuevos.link, usuario: cryptoJS.creds.usuario, uid: datosNuevos.uid}).then(()=>{
                        self.notify(window.lang.alert.pub_link_modified, 1000);
                    });
                }).catch(e=>{
                    console.error(e);
                    setTimeout(()=>{
                        vex.dialog.alert(e.toString());
                    }, 500);
                });
            }
        },
        publicar: async function(target){
        //.links-publicar
            let tgt=$(target);
            let datos={
                usuario: cryptoJS.creds.usuario,
                uid: tgt.parent().attr("data-uid")
            };
            datos.id=exports.clases.Vik.indexOfProp({prop: "uid", array: self.main.home.db.links, val: datos.uid});
            datos.link=main.home.db.links[datos.id].link;
            let res=await socketctl.enviar({tipo: "get-link", msg:{link: datos.uid}, usuario: cryptoJS.creds.usuario});
            if(res==false){
                return vex.dialog.confirm({
                    message: window.lang.alert.confirm_public,
                    callback: a=>{
                        if(!a)return self.notify(window.lang.alert.wont_change, 500);
                        return main.público("set-public", datos).then(a=>{
                            if(a.err) throw a.err;
                            if(typeof datos.id == "number"){
                                main.home.db.links[datos.id].shared=new URL(`/${datos.uid}`, window.shortDomain).href;
                            }
                            return main.home.reinsertar().then(()=>{
                                return vex.dialog.alert(window.lang.alert.short_ok.replace("{{short}}",new URL(`/${datos.uid}`, window.shortDomain)));
                            });
                        });
                    }
                });
            }else{
                return vex.dialog.confirm({
                    message: window.lang.alert.confirm_del_public,
                    callback: a=>{
                        if(!a)return self.notify(window.lang.alert.wont_change, 500);
                        return main.público("del-public", datos).then(a=>{
                            if(a.err) throw a.err;
                            delete main.home.db.links[datos.id].shared;
                            return main.home.reinsertar().then(()=>{
                                return self.notify(window.lang.alert.unshare_ok, 500);
                            });
                        });
                    }
                });
            }
        },
        limpiar: function(obj){
            if(typeof obj!="object")return obj;
            let tname=obj.item.target.tagName.toString().toLowerCase();
            if(tname==obj.tag)return obj.item.target;
            return obj.item.path[obj.up];
        },
        compartir(target){
            if(!navigator.share)return self.notify(window.lang.errors.share_not_supported);
            let datos=main.home.db.links[self.exports.clases.Vik.indexOfProp({prop: "uid", array: main.home.db.links, val: $(target).parent().attr("data-uid")})];
            let data={
                title: datos.descripción,
                url: datos.shared||datos.link
            };
            self.navigator.share(data).catch(e=>{
                console.error(e);
                self.notify(`${window.lang.error.share_error}: ${event.toString()}`);
            });
        }
    }
}
