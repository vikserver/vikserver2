/* globals Vik:false */
let index=[];
let fullIndex=[
    {abbr: "es", completo: "Español - Spanish"},
    {abbr: "en", completo: "English - English"}
];
fullIndex.forEach(a=>{
    index.push(a.abbr);
});
let lang;
if(index.indexOf(localStorage.lang)>-1){
    lang=localStorage.lang;
}else if(index.indexOf(navigator.language)>-1){
    lang=navigator.language;
}else{
    lang=index[0];
}
let idioma;
window.loadLang=function(a){
    idioma=(a||lang);
    if(window.lang) return Promise.resolve(window.lang);
    return load("lang/"+idioma+".json", ["asplain", "noprefix"])
        .then(b=>{
            window.lang=JSON.parse(b.text);
            document.querySelector("html").lang=idioma;
            return window.lang;
        });
}
Promise.all([load(["core/clases.js"]), self.loadLang()]).then(()=>{
    Vik.get("cont/modal-idiomas.html").then(a=>{
        $("#contenedor-modales").append(a);
        window.langItems.modals.idiomas=new Vue({
            el: "#modal-idiomas",
            data: {
                lang: window.lang,
                idiomas: fullIndex
            }
        });
        $("[data-idiomas='boton-cambio']").on("click", event=>{
            localStorage.setItem("lang", $(event.target).parent().attr("data-idioma"));
            location.reload();
        });
        let modalIdiomas = new mdc.dialog.MDCDialog(document.querySelector("#modal-idiomas"))
        $("#boton-idiomas").on("click", ()=>{
            modalIdiomas.open();
        });
        //         $("[data-idiomas='idioma']").on("click", function(){
        //            $("[data-idiomas='idioma']").removeClass("active");
        //            $(this).addClass("active");
        //         });
        $("[data-idiomas='guardar']").on("click", function(){
            localStorage.setItem("lang", $("[data-idiomas='idioma'].active").attr("data-idioma"));
            location.reload();
        });
    });
});

